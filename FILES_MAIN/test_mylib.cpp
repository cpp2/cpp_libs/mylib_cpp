//
// Created by lukas on 16.02.21.
//


#include <myLib_wrapper.hpp>

void myLibLogTest(int argc, char *argv[], int a, int b, int c, int d)
{
    auto vm = myLib::parseCommandlineArguments(argc, argv);

    auto log_options = myLib::parseLoggerArguments(vm);
//    log_options.setFileLogging(true);
    std::cout << log_options << std::endl;

    myLib::initLogger(log_options);
    myLib::test_log();

    LOG(output) << "argc: " << argc << std::endl;
    for (int i = 0; i<argc; i++)
    {
        std::string s = std::string(argv[i]);
        LOG(output) << "arg " << i << ": " << s << std::endl;
    }
}

int main (int argc, char *argv[])
{
    myLibLogTest(argc, argv, 1, 2, 3, 4);
}