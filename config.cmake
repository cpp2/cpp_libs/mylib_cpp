
set(CMAKE_VERBOSE_MAKEFILE 0)
set(_LIB_NAME "${_BASEDIR}")
set(_LIB_TYPE STATIC)
set(INCLUDE_PATH "/home/lukas/CODE/cpp/cpp_libs/include")

FUNCTION(CMAKE_CUSTOM)
    findlib(BOOST Boost " " log timer program_options)
    list(APPEND _ADD_LIBS ${BOOST})

    message(STATUS "Linked Libs are: ${_ADD_LIBS}")

    add_subdirectory(FILES_LIB)
    add_subdirectory(FILES_MAIN)
    add_subdirectory(TEST)
endfunction()
