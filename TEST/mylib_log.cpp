#include "gtest/gtest.h"

#include <myLib_wrapper.hpp>


void logTest_fileLoggingFatal()
{
    auto log_options = myLib::getStandardLoggerOptions();
    log_options.setVerboseLevel("fatal");
    log_options.setFileLogging(true);
    myLib::initLogger(log_options);
    myLib::test_log();
}

TEST(mylibtests, log)
{
    std::cout << "test" << std::endl;
    ASSERT_NO_THROW(logTest_fileLoggingFatal());
}

void logTest_fileLoggingNoFileNoConsole()
{
    auto log_options = myLib::getStandardLoggerOptions();
    log_options.setFileLogging(false);
    log_options.setConsoleLogging(false);
    std::cout << log_options << std::endl;
    myLib::initLogger(log_options);
    myLib::test_log();
}

TEST(mylibtests, log_noFile_noConsole)
{
    std::cout << "test" << std::endl;
    EXPECT_DEATH(logTest_fileLoggingNoFileNoConsole(), "");
}

void logTest_simulateCommandlineArguments()
{
    int argc = 3;
    char *argv[] = {(char*)"bin/unit_tests", (char*)"-v", (char*)"trace"};

    auto vm = myLib::parseCommandlineArguments(argc, argv);

    auto log_options = myLib::parseLoggerArguments(vm);
    std::cout << log_options << std::endl;

    myLib::initLogger(log_options);
    myLib::test_log();
}

TEST(mylibtests, log_commandline)
{
    std::cout << "test" << std::endl;
    ASSERT_NO_THROW(logTest_simulateCommandlineArguments());
}

void log_wrongLvlInput1()
{
    auto log_options = myLib::getStandardLoggerOptions();
    log_options.setVerboseLevel(7);}

TEST(mylibtests, log_wrong_vlvl_input1)
{
    std::cout << "test" << std::endl;
    ASSERT_THROW(log_wrongLvlInput1(), myLib::preLogFatalException);
}

void log_wrongLvlInput2()
{
    auto log_options = myLib::getStandardLoggerOptions();
    log_options.setVerboseLevel("asdf");}

TEST(mylibtests, log_wrong_vlvl_input2)
{
    std::cout << "test" << std::endl;
    ASSERT_THROW(log_wrongLvlInput2(), myLib::preLogFatalException);
}
