
#include "gtest/gtest.h"

#include <myLib_wrapper.hpp>

void timer_test()
{
    TIMER_LOCAL_INIT

    TIMER_MESSAGE("time1: ", LOG(output))

    TIMER_LOCAL_RESET

    TIMER_MESSAGE("time2: ", LOG(output))

    std::cout << "global time (fixed precision): " << MYTIMER_FIXED_PRECISION(3, TIMER_LOCAL_time) << std::endl;

    myLib::timerRestart();
}

TEST(mylibtests, timer)
{
    ASSERT_NO_THROW(timer_test());
}