#include "gtest/gtest.h"

#include <myLib_wrapper.hpp>


void fileOperations()
{
    std::string path = "testFile";
    myLib::writeToFile(path, "asdf");
    myLib::fileExists(path);
    auto perms = std::filesystem::status(path).permissions();
    std::stringstream permOutput = myLib::perms_string(perms);
    std::cout << permOutput.str() << std::endl;
}

TEST(mylibtests, fileOperations)
{
    std::cout << "test" << std::endl;
    EXPECT_NO_THROW(fileOperations());
}
