/*!
 * \file argumentParserExample.hpp
 * \brief TODO doc: COMMENT THIS!
 */


#pragma once

#include <iostream>
#include <string>

#include <boost/program_options.hpp>

#include "./myExceptions.hpp"
#include "./myLogger.hpp"


namespace myLib
{
    boost::program_options::options_description getLoggingProgramArguments();

    boost::program_options::variables_map parseCommandlineArguments(int ac, char* av[], \
    boost::program_options::options_description (*getProgramOptionsFcnPtr)() = getLoggingProgramArguments);

    logOptions parseLoggerArguments(boost::program_options::variables_map vm);

    logOptions getStandardLoggerOptions();

    void initLogger(logOptions log_options = logOptions());

    boost::program_options::variables_map initArguments(int ac, char* av[], boost::program_options::options_description (*getProgramOptionsFcnPtr)());

}
