/*!
 * \file myLogger.hpp
 * \brief TODO doc: COMMENT THIS!
 */

#pragma once

#include <cstdlib>
#include <fstream>
#include <ostream>

#include <boost/log/attributes.hpp>
#include <boost/log/common.hpp>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/sources/severity_feature.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/formatter_parser.hpp>
#include <boost/core/null_deleter.hpp>
#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/thread.hpp>

#include "./myFileToolbox.hpp"

namespace logging = boost::log;
namespace attrs = boost::log::attributes;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;
namespace expr = boost::log::expressions;
namespace keywords = boost::log::keywords;

// TODO should this be inside myLib?
enum my_severity_level
{
    trace,
    debug,
    info,
    output,
    warning,
    error,
    fatal
};

namespace myLib
{
    struct logOptions {
    public:
        logOptions();

        my_severity_level getVerboseLevel() const;

        void setVerboseLevel(my_severity_level verboseLevel);
        void setVerboseLevel(int verboseLevel);
        void setVerboseLevel(std::string verboseLevel);

        const std::string &getVerboseDir() const;

        void setVerboseDir(const std::string &verboseDir);

        bool getFileLogging() const;

        void setFileLogging(bool fileLoggingIn);

        bool getConsoleLogging() const;

        void setConsoleLogging(bool consoleLoggingIn);

        friend std::ostream &operator<<(std::ostream &os, const logOptions &options);

    private:
        my_severity_level verboseLevel;
        std::string verboseDir;
        bool fileLogging;
        bool consoleLogging;

    };



    BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", my_severity_level)

// Initializing global boost::log logger
    typedef boost::log::sources::severity_channel_logger_mt<my_severity_level, std::string> global_logger_type;

    BOOST_LOG_INLINE_GLOBAL_LOGGER_INIT(global_logger, global_logger_type)
    {
        return global_logger_type(boost::log::keywords::channel = "global_logger");
    }


#define LOG(level) \
    LOG_LOCATION; \
    BOOST_LOG_SEV(myLib::global_logger::get(), level)

#define LOG_LOCATION                            \
    boost::log::attribute_cast<boost::log::attributes::mutable_constant<int>>(boost::log::core::get()->get_global_attributes()["Line"]).set(__LINE__); \
    boost::log::attribute_cast<boost::log::attributes::mutable_constant<std::string>>(boost::log::core::get()->get_global_attributes()["File"]).set(__FILE__); \
    boost::log::attribute_cast<boost::log::attributes::mutable_constant<std::string>>(boost::log::core::get()->get_global_attributes()["Function"]).set(__func__);

// The operator puts a human-friendly representation of the severity level to the stream
    std::ostream& operator<< (std::ostream& strm, my_severity_level level);

// initializes the logger
    void _initLogger(logOptions log_options);

    void test_log();
}


