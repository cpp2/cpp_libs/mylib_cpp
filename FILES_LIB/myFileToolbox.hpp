/*!
 * \file myFileToolbox.hpp
 * \brief TODO doc: COMMENT THIS!
 */


#pragma once


#include <iostream>
#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <fstream>
#include <filesystem>
#include <omp.h>
#include <string>

#include "./myExceptions.hpp"

namespace myLib
{
    namespace fs = std::filesystem;

    /// get file/dir permissions in human readable style
    std::stringstream perms_string(fs::perms p);

    ///Prüfe, ob Dateien existieren
    bool fileExists(const std::string filePath);

    /// create / write to file
    bool writeToFile(std::string path, std::string s);

    ///Prüfe, ob Ordner existieren
    bool directoryExists( const std::string dirPath);

    ///Erzeuge Ordner
    bool createDirectory( const std::string dirPath);

}



