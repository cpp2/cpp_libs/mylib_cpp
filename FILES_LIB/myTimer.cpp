
#include "myTimer.hpp"


namespace myLib
{

void timerRestart()
{
    timer.start();
}

float getElapsed()
{
    last_global_timed = static_cast<float>(myLib::timer.elapsed().wall);
    return last_global_timed/1000000000.0f;
}

}
