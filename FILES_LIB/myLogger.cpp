
#include "myLogger.hpp"

namespace myLib
{

// The operator puts a human-friendly representation of the severity level to the stream
    std::ostream& operator<< (std::ostream& strm, my_severity_level level)
    {
        const std::string my_severity_level_strings[7] =
                {
                        "trace",
                        "debug",
                        "info",
                        "output",
                        "warning",
                        "error",
                        "fatal"
                };

        if (static_cast< std::size_t >(level) < sizeof(my_severity_level_strings) / sizeof(*my_severity_level_strings))
            strm << std::setw(7) << my_severity_level_strings[level];
        else
            strm << static_cast< int >(level);

        return strm;
    }




    my_severity_level logOptions::getVerboseLevel() const {
        return verboseLevel;
    }

    void logOptions::setVerboseLevel(my_severity_level verboseLevel) {
        logOptions::verboseLevel = verboseLevel;
    }
    void logOptions::setVerboseLevel(int verboseLevel)
    {
        if(!(verboseLevel >= 0 && verboseLevel <= 6))
        {
            throw preLogFatalException("Only verbose levels from 0 to 6 are accepted. ");
        }
        setVerboseLevel(static_cast<my_severity_level>(verboseLevel));
    }
    void logOptions::setVerboseLevel(std::string verboseLevel) {
        int intVerboseLevel;
        try {
            intVerboseLevel = boost::lexical_cast<int>(verboseLevel);
        }
        catch(std::exception &e)
        {
            boost::algorithm::to_lower(verboseLevel);
            if(verboseLevel == "trace") { intVerboseLevel = 0; }
            else if(verboseLevel == "debug") { intVerboseLevel = 1; }
            else if(verboseLevel == "info") { intVerboseLevel = 2; }
            else if(verboseLevel == "output") { intVerboseLevel = 3; }
            else if(verboseLevel == "warning") { intVerboseLevel = 4; }
            else if(verboseLevel == "error") { intVerboseLevel = 5; }
            else if(verboseLevel == "fatal") { intVerboseLevel = 6; }
            else{
                throw preLogFatalException("unexpected input for verbose lvl");
            }
        }
        setVerboseLevel(intVerboseLevel);
    }

    const std::string &logOptions::getVerboseDir() const {
        return verboseDir;
    }

    void logOptions::setVerboseDir(const std::string &verboseDirIn) {
        verboseDir = verboseDirIn;
    }

    bool logOptions::getFileLogging() const {
        return fileLogging;
    }

    void logOptions::setFileLogging(bool fileLoggingIn) {
        fileLogging = fileLoggingIn;
    }

    bool logOptions::getConsoleLogging() const {
        return consoleLogging;
    }

    void logOptions::setConsoleLogging(bool consoleLoggingIn) {
        consoleLogging = consoleLoggingIn;
    }

    logOptions::logOptions()
    {
        setVerboseLevel("output");
        setConsoleLogging(true);
        setFileLogging(false);
        setVerboseDir("log");
    }

    std::ostream &operator<<(std::ostream &os, const logOptions &options) {
        os << "verboseLevel: " << options.verboseLevel << " verboseDir: " << options.verboseDir
        << " fileLogging: " << options.fileLogging << " consoleLogging: " << options.consoleLogging;
        return os;
    }

    void _initLogger(logOptions log_options)
    {
        const std::string my_severity_colors[7] =
                {
                        "\033[01;90m",
                        "\033[01;34m",
                        "\033[01;32m",
                        "\033[0m",
                        "\033[01;33m",
                        "\033[01;31m",
                        "\033[5;1;31m"
                };

        const std::string reset_color = "\033[0m";

        logging::core::get()->add_global_attribute("Line", attrs::mutable_constant<int>(5));
        logging::core::get()->add_global_attribute("File", attrs::mutable_constant<std::string>(""));
        logging::core::get()->add_global_attribute("Function", attrs::mutable_constant<std::string>(""));

#define LOCFORMAT "   (" << expr::attr<std::string>("File") << "(" << expr::attr<int>("Line") << "): " << expr::attr<std::string>("Function") << ")"


        auto fileFormatWithLocation = expr::stream
                << expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d %H:%M:%S   ")
                << std::setw(7) << severity << "  "
                << expr::smessage << LOCFORMAT;

        auto fileFormatStd = expr::stream
                << expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d %H:%M:%S   ")
                << std::setw(3) << severity << "  "
                << expr::smessage << LOCFORMAT;

        auto coloredFormat = expr::stream
                << expr::if_(severity==trace)[expr::stream << my_severity_colors[trace] << expr::smessage << reset_color ] \
        << expr::if_(severity==debug)[expr::stream << my_severity_colors[debug] << expr::smessage << reset_color ] \
        << expr::if_(severity==info)[expr::stream << my_severity_colors[info] << expr::smessage << reset_color ] \
        << expr::if_(severity==output)[expr::stream << my_severity_colors[output] << expr::smessage << reset_color ] \
        << expr::if_(severity==warning)[expr::stream << my_severity_colors[warning] << expr::smessage << reset_color ] \
        << expr::if_(severity==error)[expr::stream << my_severity_colors[error] << expr::smessage << reset_color ] \
        << expr::if_(severity==fatal)[expr::stream << my_severity_colors[fatal] << expr::smessage << reset_color ] ;


        // Initialize sinks
        typedef sinks::synchronous_sink< sinks::text_ostream_backend > text_sink;
        boost::shared_ptr< text_sink > sink = boost::make_shared< text_sink >();

        sink->locked_backend()->auto_flush(true);

        if(!log_options.getConsoleLogging() && !log_options.getFileLogging())
        {
            throw preLogFatalException("Neither console nor file logging is enabled!");
        }
        if(log_options.getFileLogging())
        {
            myLib::createDirectory(log_options.getVerboseDir());

            sink = boost::make_shared< text_sink >();
            sink->locked_backend()->add_stream(boost::make_shared<std::ofstream>(log_options.getVerboseDir()+"/all.log"));
            sink->set_formatter(fileFormatStd);
            sink->set_filter(expr::attr<my_severity_level>("Severity") >= trace);
            logging::core::get()->add_sink(sink);

            sink = boost::make_shared< text_sink >();
            sink->locked_backend()->add_stream(boost::make_shared<std::ofstream>(log_options.getVerboseDir()+"/std.log"));
            sink->set_formatter(fileFormatStd);
            sink->set_filter(expr::attr<my_severity_level>("Severity") >= log_options.getVerboseLevel());
            logging::core::get()->add_sink(sink);
        }

        if(log_options.getConsoleLogging())
        {
            sink = boost::make_shared< text_sink >();
            sink->locked_backend()->add_stream(boost::shared_ptr<std::ostream>(&(std::cout), boost::null_deleter()));
            sink->set_formatter(coloredFormat);
            sink->set_filter(expr::attr<my_severity_level>("Severity") >= log_options.getVerboseLevel());
            logging::core::get()->add_sink(sink);
        }


        logging::add_common_attributes();
    }

    void test_log()
    {
        LOG(trace)    << "trace"      << " severity message.";
        LOG(debug)    << "debug"      << " severity message.";
        LOG(info)     << "info"       << " severity message.";
        LOG(output)   << "output"     << " severity message.";
        LOG(warning)  << "warning"    << " severity message.";
        LOG(error)    << "error"      << " severity message.";
        LOG(fatal)    << "fatal"      << " severity message.";
    }

}

