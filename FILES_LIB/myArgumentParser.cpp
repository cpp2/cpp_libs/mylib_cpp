
#include "myArgumentParser.hpp"

namespace myLib
{
    boost::program_options::options_description getLoggingProgramArguments()
    {
        boost::program_options::options_description desc("Allowed options");

        desc.add_options() ("help,h", "produce help message") \
        ("verboselevel,v", boost::program_options::value<std::string>()->default_value("3"), "verbose level: only verbose greater or equal than <verboselevel> will be logged. (trace = 0, debug, info, output, warning, error, fatal=6)") \
        ("logdirectory", boost::program_options::value<std::string>()->default_value("log"), "verbosefile directory") \
        ("enablefilelog", boost::program_options::bool_switch()->default_value(false), "enable the log to files") \
        ("disableconsolelog", boost::program_options::bool_switch()->default_value(false), "disable the log to console");

        return desc;
    }

    boost::program_options::variables_map parseCommandlineArguments(int ac, char* av[], boost::program_options::options_description (*getProgramOptionsFcnPtr)())
    {
        boost::program_options::variables_map vm;

        try
        {
            vm = initArguments(ac, av, getProgramOptionsFcnPtr);
        }
        catch (preLogSoftExitException &e)
        {
            exit(EXIT_SUCCESS);
        }
        catch (preLogFatalException &e)
        {
            std::cerr << e.what() << std::endl;
            exit(EXIT_FAILURE);
        }
        catch(std::exception &e)
        {
            std::cerr << "EXCEPTION: " << e.what() << std::endl;
            exit(EXIT_FAILURE);
        }
        catch (...)
        {
            std::cerr << "UNKNOWN ERROR - EXIT PROGRAM\n";
            exit(EXIT_FAILURE);
        }

        return vm;
    }

    logOptions parseLoggerArguments(boost::program_options::variables_map vm)
    {
        logOptions log_options = logOptions();

        try {
            log_options.setVerboseLevel(vm["verboselevel"].as<std::string>());
            log_options.setConsoleLogging(!vm["disableconsolelog"].as<bool>());
            log_options.setFileLogging(vm["enablefilelog"].as<bool>());
            log_options.setVerboseDir(vm["logdirectory"].as<std::string>());
        } catch (...)
        {
            std::cerr << "COMMANDLINE LOGGER ARGS PARSING PROBLEM" << std::endl;
        }

        return log_options;
    }

    logOptions getStandardLoggerOptions()
    {
        return logOptions();
    }

    void initLogger(logOptions log_options)
    {
        try
        {
            myLib::_initLogger(log_options);
        }
        catch(std::exception &e)
        {
            std::cerr << "PROBLEM INITIALIZING LOGGER: " << e.what() << std::endl;
            exit(EXIT_FAILURE);
        }
        catch (preLogFatalException &e)
        {
            std::cerr << e.what() << std::endl;
            exit(EXIT_FAILURE);
        }
        catch (...)
        {
            std::cerr << "UNKNOWN ERROR - EXIT PROGRAM\n";
            exit(EXIT_FAILURE);
        }
    }

    boost::program_options::variables_map initArguments(int ac, char* av[], boost::program_options::options_description (*getProgramOptionsFcnPtr)())
    {
        boost::program_options::options_description desc = getProgramOptionsFcnPtr();

        boost::program_options::variables_map vm;

        try
        {
            boost::program_options::store(boost::program_options::parse_command_line(ac, av, desc), vm);
            boost::program_options::notify(vm);
        }
        catch(std::exception &e)
        {
            throw preLogFatalException(e.what());
        }

        if (vm.count("help"))
        {
            std::cout << desc << "\n";
            throw preLogSoftExitException();
        }

        return vm;
    }

}



