/*!
 * \file myTimer.hpp
 * \brief TODO doc: COMMENT THIS!
 */


#pragma once

#include <boost/timer/timer.hpp>


namespace myLib
{

static boost::timer::cpu_timer timer;

static float last_global_timed;

static float splitTime;


void timerRestart();

float getElapsed();

}


#define TIMER_LOCAL_INIT \
    float TIMER_LOCAL_start      = myLib::getElapsed(); \
    float TIMER_LOCAL_time       = 0.0; \
    float TIMER_LOCAL_splitTime  = 0.0; \
    {float asdf = TIMER_LOCAL_start + TIMER_LOCAL_time + TIMER_LOCAL_splitTime; asdf = 0.0; TIMER_LOCAL_time = asdf;}


#define TIMER_LOCAL_RESET \
    TIMER_LOCAL_start      = myLib::getElapsed(); \
    TIMER_LOCAL_time       = 0.0; \
    TIMER_LOCAL_splitTime  = 0.0;


#define TIMER_LOCAL_STOP \
    TIMER_LOCAL_splitTime = myLib::getElapsed() - TIMER_LOCAL_time - TIMER_LOCAL_start; \
    TIMER_LOCAL_time = myLib::getElapsed() - TIMER_LOCAL_start;


#define TIMER_MESSAGE(message, stream) \
    TIMER_LOCAL_STOP\
    stream << message << TIMER_LOCAL_splitTime << " (total: " << TIMER_LOCAL_time << ")";



#define MYTIMER_FIXED_PRECISION(prec, arg)    std::fixed << std::setprecision( prec ) << std::setfill( '0' ) << arg << std::defaultfloat


