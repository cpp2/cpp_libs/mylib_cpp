/*!
 * \file myExceptions.hpp
 * \brief TODO doc: COMMENT THIS!
 */


#pragma once

#include <exception>
#include <string>


namespace myLib
{

struct myException : public std::exception
{
    myException():msg("MY EXCEPTION"){}
    myException(const std::string msg):msg(msg){}

    const char * what () const noexcept{ return msg.c_str(); }

private:
    std::string msg;
};




struct preLogException : public std::exception
{
};




struct softExitException : public std::exception
{
};




struct fatalException : public myException
{
    fatalException():myException("FATAL EXCEPTION"){}
    fatalException(const std::string msg):myException(msg){}
};


struct errorException : public myException
{
    errorException():myException("ERROR EXCEPTION"){}
    errorException(const std::string msg):myException(msg){}
};


struct warningException : public myException
{
    warningException():myException("WARNING EXCEPTION"){}
    warningException(const std::string msg):myException(msg){}
};




struct preLogFatalException : public preLogException, public fatalException
{
    preLogFatalException():fatalException("FATAL ERROR HAPPENED BEFORE LOG INITIALIZATION"){}
    preLogFatalException(const std::string msg):fatalException(msg){}

    using myException::what;
};


struct preLogSoftExitException : public preLogException, public softExitException
{
};




}




