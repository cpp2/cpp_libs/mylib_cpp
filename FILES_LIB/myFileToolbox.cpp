
#include "myFileToolbox.hpp"

namespace myLib
{

    std::stringstream perms_string(fs::perms p)
    {
        std::stringstream of;

        of << ((p & fs::perms::owner_read) != fs::perms::none ? "r" : "-")
           << ((p & fs::perms::owner_write) != fs::perms::none ? "w" : "-")
           << ((p & fs::perms::owner_exec) != fs::perms::none ? "x" : "-")
           << ((p & fs::perms::group_read) != fs::perms::none ? "r" : "-")
           << ((p & fs::perms::group_write) != fs::perms::none ? "w" : "-")
           << ((p & fs::perms::group_exec) != fs::perms::none ? "x" : "-")
           << ((p & fs::perms::others_read) != fs::perms::none ? "r" : "-")
           << ((p & fs::perms::others_write) != fs::perms::none ? "w" : "-")
           << ((p & fs::perms::others_exec) != fs::perms::none ? "x" : "-");

        return of;
    }

    bool fileExists(std::string filePath)
    {
        std::ifstream infile(filePath);
        return infile.good();
    }

    bool writeToFile(std::string path, std::string s) {

        std::ofstream ofs(path);
        ofs << s;
        ofs.close();

        return 0;
    }

    bool directoryExists( const std::string dirPath){
        DIR *pDir;
        bool bExists = false;

        pDir = opendir (dirPath.c_str());

        if (pDir != NULL){
            bExists = true;
            closedir (pDir);
        }
        return bExists;
    }

    bool createDirectory( const std::string dirPath )
    {
        if(directoryExists(dirPath))
            return 0;

        const bool dir_err = fs::create_directory(dirPath.c_str());

        if (dir_err == false)
        {
            throw errorException("Error creating directory!");
        }
        return 0;
    }

}



