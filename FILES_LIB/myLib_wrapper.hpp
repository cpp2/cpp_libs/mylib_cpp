#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"

#include "myTimer.hpp"
#include "myExceptions.hpp"
#include "myArgumentParser.hpp"
#include "myLogger.hpp"
#include "myFileToolbox.hpp"

#pragma GCC diagnostic pop
